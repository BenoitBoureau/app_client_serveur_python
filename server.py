import socket, sys, threading, time, random

#************************************************#
#**************** INITIALISATION ****************#
#************************************************#


#****************** VARIABLES *******************#


HOTE = ""
PORT = 50026

NOMBREJOUEUR = 2

# Durée max question ; en secondes
dureemax = 10

# On patiente 3 secondes entre chaques question
pause = 3

# Stockage des connexions clients
dict_clients = {}

# Dictionnaire des pseudos
dict_pseudos = {}

# Dictionnaire des réponses des clients
dict_reponses = {}

# Dictionnaire des scores de la dernière question
dict_scores = {}

# Dictionnaire des scores totaux
dict_scores_total = {}

# On crée un tableau "liste de question" avec le question en clé et la bonne réponse en valeur
liste_de_question = []

# On crée un tableau pour stocker 10 questions parmis le lôt
liste_de_question_selectionnes = []


#****************** QUESTIONS *******************#


question = """
Quelle pièce est absolument à protéger dans un jeu d'échec ?
    1) La tour
    2) La reine
    3) Le roi"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
Quel est la capitale de l'Australie ?
    1) Canberra
    2) Melbourne
    3) Sydney"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
Quelle année a suivi l'an 1 avant JC ?
    1) L'an 2 avant JC
    2) L'an 0
    3) L'an 1 après JC"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
En quelle année est mort JFK ?
    1) 1953
    2) 1963
    3) 1973"""

reponse = 2
liste_de_question.append((question,reponse))

question = """
Quel est l'équivalent du pape au Tibet ?
    1) Le Dallai Chameau
    2) Le Dallai Lama
    3) Le Dallai Dromadaire"""

reponse = 2
liste_de_question.append((question,reponse))

question = """
Quelle est la voiture dans Retour vers le futur ?
    1) La Doloréanne
    2) La twingo de 2004
    3) La Tesla"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
Comment s'appelle l'équivalent du musée Grévin à Londres ?
    1) Musée Oller
    2) Musée Hartkoff
    3) Madame Tussauds"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
Selon l'expression, quel animal a t'on au plafond quand on est un peu fou ?
    1) Un cochon
    2) Une araignée
    3) Un phasme"""

reponse = 2
liste_de_question.append((question,reponse))

question = """
Qui est l'inventeur du mythique pas "Moon Walk" ?
    1) Michael Jackson
    2) Michael Jordan
    3) Mickael Schumacher"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
De quelle couleur est le cheval blanc d'Henri IV ?
    1) Blanc
    2) Gris
    3) Alezan"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
Quel est le 2ème nom de l'hippocampe ?
    1) Le cheval de mer
    2) Le poney de mer
    3) La jument de mer"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
Quel précipités observe-t-on quand on mélange du nitrate d'argent avec du chlore ?
    1) Précipité blanc qui se noircit
    2) Précipité bleu qui blanchit
    3) Ca explose"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
Quel ville est surnommée 'Big Apple' ?
    1) Boston
    2) Los angeles
    3) New York"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
Comment appelle-t-on la lumière qui se rapproche le plus de la lumière du soleil ?
    1) Blanche
    2) Eblouissante
    3) Rouge"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
Si ce n'est pas un fruit, qu'est-ce qu'un kiwi?
    1) Un oiseau
    2) Un art martial
    3) Un outil"""

reponse = 1
liste_de_question.append((question,reponse))

question = """
 Quel nom porte le siège de la police londonienne ?
    1) Interpole
    2) Scotland Yard
    3) 36 quai des orfèvres"""

reponse = 2
liste_de_question.append((question,reponse))

question = """
Qu'est-ce que la "Licorne" dont Tintin perce le secret ?
    1) Un animal légendaire
    2) Un cheval avec une carotte
    3) Un bateau"""

reponse = 3
liste_de_question.append((question,reponse))

question = """
Au hockey sur glace, où doit aller un joueur ayant reçu une pénalité ?
    1) En prison
    2) Sur le banc de touche
    3) Au vestiaire"""

reponse = 1

question = """
De quelle région fait partie le département de l'Ain ? 
    1) Ile de france
    2) Rhône-Alpes 
    3) Creuse"""

reponse = 2

question = """
Quelle profession est concernée par le serment d'Hippocrate ? 
    1) Médecine
    2) Avocat
    3) Maconnerie"""

reponse = 1

question = """
Combien d'étoiles comporte le drapeau américain ?
    1) 45
    2) 50
    3) 55"""

reponse = 2

question = """
Quel "super-héros" est amoureux de Loïs Lane ?
    1) Superman
    2) Batman
    3) Iron man"""

reponse = 1

question = """
A quel compositeur doit-on la célèbre "Chevauchée des Walkyries" ? 
    1) Wagner 
    2) Liszt
    3) Schopenhauer"""

reponse = 1
liste_de_question.append((question,reponse))


# On sélectionne 10 question parmis toutes les questions
while len(liste_de_question_selectionnes) != 10:
    num_question = random.randint(0, len(liste_de_question) -1)
    if(liste_de_question[num_question] not in liste_de_question_selectionnes):
        liste_de_question_selectionnes.append(liste_de_question[num_question])


#************************************************#
#******************** CLASS *********************#
#************************************************#

# Class ThreadClient
# @param - On envoie la variable "connexion" avec la connexion acceptée
class ThreadClient(threading.Thread):
    
    def __init__(self,conn):

        threading.Thread.__init__(self)
        self.connexion = conn
        
        # Mémoriser la connexion dans le dictionnaire
        
        self.nom = self.getName() # identifiant du thread "<Thread-N>"
        dict_clients[self.nom] = self.connexion
        dict_scores[self.nom] = 0
        dict_scores_total[self.nom] = 0
        
        print("Connexion du client", self.connexion.getpeername(),self.nom ,self.connexion)
        
        message = bytes("Vous êtes connecté au serveur.\n","utf-8")
        self.connexion.send(message)
        
        
    def run(self):
        
        # Choix du pseudo    
        
        self.connexion.send(b"Entrer un pseudo :\n")
        # attente réponse client
        pseudo = self.connexion.recv(1024)
        pseudo = pseudo.decode(encoding='UTF-8')
        
        dict_pseudos[self.nom] = pseudo
        
        print("Pseudo du client", self.connexion.getpeername(),">", pseudo)
        
        message = b"\nAttente des autres clients...\n"

        MessagePourTous("\n"+ pseudo + " vient de rejoindre la partie ! \n")

        self.connexion.send(message)
    
        # Réponse aux questions
       
        while True:
            
            try:
                # attente réponse client
                reponse = self.connexion.recv(1024)
                reponse = reponse.decode(encoding='UTF-8')

                if reponse == "fin":
                    del dict_pseudos[self.nom]
                    del dict_clients[self.nom]
                    del dict_scores_total[self.nom]
                    MessagePourTous(pseudo + " vient de quitter la partie ! \n")
                    # MessagePourTous(len(dict_pseudos) + " joueur(s) restant(s) ! \n")

                if reponse == "jouer":
                    dict_scores_total.clear
                    liste_de_question_selectionnes.clear
                    play = 1
                
                if reponse == "stop":
                    for client in dict_clients:
                        dict_clients[client].close()
                        print("Déconnexion du socket", client)

                    input("\nAppuyer sur Entrée pour quitter l'application...\n")
                    



            except:
                # fin du thread
                break
                
            if self.nom not in dict_reponses:
                dict_reponses[self.nom] = reponse, time.time()
                print("Réponse du client",self.nom,">",reponse)


        # print("\nFin du thread",self.nom)
        # self.connexion.close()



#************************************************#
#****************** DEFINITION ******************#
#************************************************#


def MessagePourTous(message):
    """ message du serveur vers tous les clients"""
    for client in dict_clients:
        dict_clients[client].send(bytes(message,"utf8"))
        



#************************************************#
#**************** FONCTIONNEMENT ****************#
#************************************************#


mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)


try:
    mySocket.bind((HOTE, PORT))
except socket.error:
    print("La liaison du socket à l'adresse choisie a échoué.")
    sys.exit()


print("Serveur prêt (port",PORT,") en attente de joueurs...")

mySocket.listen(5)


#*********** VERIFICATION NB JOUEURS ************#


# Tant que notre nombre de client actuel est inférieur à notre nombre de joueur demandé
while len(dict_clients) < NOMBREJOUEUR:
    try:
        connexion, adresse = mySocket.accept()
    except:
        sys.exit()

    th = ThreadClient(connexion)
    th.setDaemon(1)
    th.start()


#*********** VERIFICATION NB PSEUDO *************#


# Tant que notre nombre de pseudo ajouté est inférieur à notre nombre de joueur demandé
while len(dict_pseudos) < NOMBREJOUEUR:
    # on attend que tout le monde ait entré son pseudo
    pass

#********** PSEUDO OK - DEBUT PARTIE ************#

play = 1

while play == 1:

    play = 0

    # questions
    index = 0

    MessagePourTous("\nCe questionnaire est un QCM, veuillez entrer le numéro de la réponse !")
    time.sleep(3)
    MessagePourTous("Vous aurez "+ str(dureemax) +" secondes pour répondre !")
    time.sleep(3)
    MessagePourTous("La partie va commencer...")

    time.sleep(5)

    # Pour chaques question dans la liste de question ajoutés plus tôt on envoie le timer + la question
    for question in liste_de_question_selectionnes:

        if len(dict_pseudos) < 2:

            MessagePourTous("Pas assez de joueurs pour continuer...")
            MessagePourTous("Fin de partie !\n")
            
        else:

            index += 1
            
            MessagePourTous("\nNouvelle question :\n")
            
            MessagePourTous(str(pause) + " secondes...")
            time.sleep(1)

            MessagePourTous(str(pause -1) + " secondes...")
            time.sleep(1)

            MessagePourTous(str(pause -2) + " seconde...")
            time.sleep(1)

            timedebut = time.time()
            timefin = timedebut + dureemax
            dict_reponses = {}  # liste des réponses des clients
            dict_scores = {}

            
            message = """--------------------
Question """ +str(index) + "/10\n"
            message += question[0]
            message += """
Réponse > """
            MessagePourTous(message)

                
            while time.time() < timefin:
                pass

            # On récupère la réponse, position 1 du tableau (0 est là question)
            reponse = question[1]
            MessagePourTous("La réponse attendu était la "+ str(question[1]) +"\n")

            for client in dict_reponses:
                try:
                    reponseClient = int(dict_reponses[client][0])
                    if reponseClient == reponse:
                        # bonne réponse 1 pts
                        dict_scores[client] = 1
                        
                    elif reponse != 0:
                        # mauvaise réponse 0 pt
                        dict_scores[client] = 0
                        
                    else:
                        # réponse je ne sais pas 0 pt
                        dict_scores[client] = 0
                    
                except:
                    # mauvaise réponse 0 pt
                    dict_scores[client] = 0
                    
                dict_scores_total[client] += dict_scores[client] 


    message = """

Resultat :
Client      Score
""" 

    for client in dict_scores_total:
        message += "%-10s  %0d pt\n"  %(dict_pseudos[client],dict_scores_total[client])

    MessagePourTous(message)

    print("fin de partie")

    MessagePourTous("\nLe vainqueur est "+ dict_pseudos[max(dict_scores_total)]+"\n")

    MessagePourTous("Pour rejouer tapez 'jouer'\n")
    MessagePourTous("Pour arreter tapez 'stop'\n")

    MessagePourTous("\n Une absence de réponse se soldera par une fin de partie'\n")
    
    stop = 10
    for tempostop in range(stop):
        tempo = stop - tempostop
        MessagePourTous(str(tempo) + " secondes...")
        time.sleep(1)


    MessagePourTous("La partie est terminée ! Merci d'avoir joué :)\n")
    


# Fin
MessagePourTous("\nFIN\nVous pouvez fermer l'application...\n")

# fermeture des sockets
for client in dict_clients:
    dict_clients[client].close()
    print("Déconnexion du socket", client)

input("\nAppuyer sur Entrée pour quitter l'application...\n")